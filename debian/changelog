vim-pathogen (2.4-7) unstable; urgency=medium

  * Fix changelog entry

 -- Andrea Capriotti <capriott@debian.org>  Sun, 14 Aug 2022 19:16:55 +0200

vim-pathogen (2.4-6) unstable; urgency=medium

  * Migrate to dh_vim-addon (Closes: #1015945)
    + Thanks Nicholas Guriev
  * Bump Standards-Version to 4.6.1, no changes needed.
  * Dependency reduced to 'vim' again. (Closes: 960492)
    + Thanks Wolfgang Karall-Ahlborn
  * Bump watch version to 4, no changes needed.
  * d/copyright Update debian copyright file
  * Add Rules-Requires-Root

 -- Andrea Capriotti <capriott@debian.org>  Sun, 14 Aug 2022 17:36:10 +0200

vim-pathogen (2.4-5) unstable; urgency=medium

  * d/control: Remove useless dependencies.

 -- Andrea Capriotti <capriott@debian.org>  Sun, 26 Apr 2020 17:29:06 +0200

vim-pathogen (2.4-4) unstable; urgency=medium

  * New source upload

 -- Andrea Capriotti <capriott@debian.org>  Wed, 18 Mar 2020 03:22:49 +0100

vim-pathogen (2.4-3) unstable; urgency=medium

  * d/README.Debian Add note about infect line (Closes: #855322)

 -- Andrea Capriotti <capriott@debian.org>  Wed, 18 Mar 2020 02:15:55 +0100

vim-pathogen (2.4-2) unstable; urgency=medium

  * d/control: Update Standards-Version to 4.5.0
  * d/control: Switch to debhelper level 12
  * d/control: Set Vcs-* to salsa.debian.org
  * d/rules: Remove trailing whitespaces
  * d/copyright: Use https protocol in Format field
    - Thanks to Ondřej Nový
  * d/control: Use dependency with Python support. (Closes: #914583)
    - Thanks to Heinrich Schuchardt for the suggestion

 -- Andrea Capriotti <capriott@debian.org>  Wed, 18 Mar 2020 01:16:16 +0100

vim-pathogen (2.4-1) unstable; urgency=medium

  * New upstream release
  * Standard version bumped to 3.9.7
  * Removed useless preinst script
    - Thanks to Sam Morris for the suggestion

 -- Andrea Capriotti <capriott@debian.org>  Sat, 13 Feb 2016 16:09:01 +0100

vim-pathogen (2.3-3) unstable; urgency=medium

  * Fixed debian watch
  * Fixed debian copyright file
  * Set Standards-Version to 3.9.6; no changes

 -- Andrea Capriotti <capriott@debian.org>  Mon, 09 Feb 2015 16:47:15 +0100

vim-pathogen (2.3-2) unstable; urgency=low

  * Added Vcs field in debian/control

 -- Andrea Capriotti <capriott@debian.org>  Thu, 17 Jul 2014 12:53:05 +0200

vim-pathogen (2.3-1) unstable; urgency=medium

  * New upstream release.
  * Added maintainer scripts.

 -- Andrea Capriotti <capriott@debian.org>  Mon, 09 Jun 2014 17:20:43 +0200

vim-pathogen (2.2-1) unstable; urgency=low

  * Initial release (Closes: #734759)

 -- Andrea Capriotti <capriott@debian.org>  Tue, 10 Dec 2013 17:42:07 +0100
